---
layout: handbook-page-toc
title: External Virtual Events
description: An overview of external virtual events including virtual conferences where we sponsor a booth, and sponsored webinars with third party vendors.
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# Overview
{: #overview .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
---

External virtual events are, by definition, not owned and hosted by GitLab. They are hosted by an external third party (i.e. a partner or paid vendor). The goal of external virtual events is to drive net new leads, and we do not promote to our internal database. The various types of external virtual events are below, and involve epic and issue creation, designation of DRIs, and workback schedule definition within the issue due dates.

* [Partner-Hosted Webinars](/handbook/marketing/virtual-events/external-virtual-events/#partner-hosted-webinars): hosted by a channel partner (i.e. WWT), this is an unpaid tactic. The channel partner manages landing page, moderating and hosting the webinar on their platform. GitLab represents as a speaker at the event, sometimes jointly with an alliance partner. A lead list is often not shared after the event, as the channel partner will work the leads. We will sometimes promote, and determine which channels are appropriate.
* [Sponsored Webinars](/handbook/marketing/virtual-events/external-virtual-events/#sponsored-webinars): hosted on an external vendor platform (i.e. DevOps.com), this is a paid tactic. The vendor is responsible for driving registration, moderating and hosting the webinar on their platform, and delivering a lead list after the event. The goal of a sponsored webinar is net new leads - we do not promote to our existing database as it is a paid activity.
* [Virtual Conferences](/handbook/marketing/virtual-events/external-virtual-events/#virtual-conferences): hosted on an external vendor platform (i.e. Hopin), this is a paid tactic. GitLab pays a sponsorship fee to receive a virtual booth and often speaking session or panel presence. The goal of a sponsored virtual conference is net new leads - we do not promote to our existing database as it is a paid activity.
* [Executive Roundtable](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-executive-roundtable):  hosted on an external vendor platform, this is a gathering of high level CxO attendees run as an open discussion between the moderator/host, GitLab expert and delegates. There usually aren't any presentations, but instead a discussion where anyone can chime in to speak. The host would prepare questions to lead discussion topics and go around the room asking delegates questions to answer. The goal of an executive roundtable is net new leads - we do not promote to our existing database as it is a paid activity.
* [Vendor Arranged Meetings](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-vendor-arranged-meetings): hosted by an external vendor, the vendor organizes one-to-one meetings with prospect or customer accounts. This does not include meetings set internally by GitLab team members. An example would be a "speed dating" style meeting setup where a vendor organizes meetings with prospects of interest to GitLab. The goal of a venor arranged meeting is to generate meetings with accounts of interest that we are finding challenging to break into directly - we do not promote to our existing database as it is a paid activity.

# Partner Webinars
{: #partner-hosted-webinars .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
---

*A parter-hosted webinar is hosted by the partner, with the goal of driving net new leads. The partner is responsible for driving registration, moderating and hosting the webinar on their platform, and, in some cases, delivering a lead list after the event. The project owner (Partner Marketing) is responsible for creating the epic and related issue creation, and keeping timelines and DRIs up-to-date.*

### How to request Partner Marketing support
{: #partner-hosted-webinar-requesting-support}
<!-- DO NOT CHANGE THIS ANCHOR -->

CAMs and Alliance Managers should use this [issue template](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new) to request Channel Marketing support for their planned event or webinar.

#### How to view triage board of Partner Marketing Requests
{: #partner-hosted-webinar-triage-board}
<!-- DO NOT CHANGE THIS ANCHOR -->

[View Board](https://gitlab.com/groups/gitlab-com/-/boards/1779611?label_name[]=Channel&label_name[]=Channel%20Marketing)

### Process in GitLab to organize epic & issues
{: #partner-hosted-webinar-project-management}
<!-- DO NOT CHANGE THIS ANCHOR -->
The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner creates the epic to house all related issues (code below)
1. Project owner creates the relevant issues required (shortcut links in epic code below)
1. Project owner associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Epic code and issue templates
{: #partner-hosted-webinar-epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->

```
<!-- Name this epic: Channel Webinar - [Webinar Name] (Partner) - [3-letter Month] [Date], [Year] -->
<!-- Example epic name: Channel Webinar - Modern CI/CD with Anthos (WWT) - Apr 22, 2021 -->

## [Partner Marketing Request Issue >>]() `to be added`

- [ ] Once date is finalized, add to [All Marketing Calendar](https://docs.google.com/spreadsheets/d/1c2V3Aj1l_UT5hEb54nczzinGUxtxswZBhZV8r9eErqM/edit?usp=sharing)

## :notepad_spiral: Key Details 
* **Slack channel:** <!-- add slack channel # -->
* **[Meeting Notes]()** <!-- to be added by Partner Marketing -->
* **Speaker(s) and Moderator:** <!-- add gitlab handle and company -->
* **CAM:** <!-- add gitlab handle -->
* **Partner Marketing DRI:** <!-- add gitlab handle -->
* **Other GitLab Sponsors:** <!-- add gitlab handle(s) --> 
* **Partner's Marketing Liaison:** <!-- add gitlab handle / name -->
* **Marketo Program Type:** Webinar (Partner)
* **Organizer/Webinar hosting:** <!-- add who is hosting the webinar -->
* **Landing Page/Registration URL:** 
* **Persona (choose one):** `Practitioner, Manager, or Executive`
* **Sales Segment:** `Large` (GCP's focus)
* **Sales Region:** `AMER, EMEA, APAC`
* **Topic:** 
* **Event Name:** <!-- official name TBD -->
* **Event Date:** YYYY-MM-DD
* **Duration:**  (X min content + X min Q/A)
* **GTM Motion (choose primary):** `CI/CD, DevOps Platform, GitOps` 
* **Vertical (optional, if specific):** 
* **Goal:** `Please be specific on the metric this is meant to impact.`
* **Total Cost:** 
* **MDF Requested:** 

## Program Tracking <!-- Delete if not receiving leads -->
* [ ] [main SFDC campaign](tbd)
* [ ] [main Marketo program](tbd)
* [ ] [List clean and upload issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list)

## Existing Material/Assets/Presentations
- [Name of content]()

## :books: Tasks and Issues Created and Linked to Epic

## If GitLab is hosting, follow [Webcast process in handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#partner-webcasts

## General Checklist
- [ ] Make sure partner is adhering to the GitLab Branding guides and logo usage
- [ ] Provide opt in language to partner
- [ ] Review and approve all email and landing page copy
- [ ] Consider creating separate invite htmls with UTMs for each partner and GitLab to send to GitLab and the partner sales orgs to help drive attendance.  Separate UTMs will help us track registrations coming from each sales org
* [ ] Add to /events/ page - [Handbook Instructions](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents)
* [ ] [Speaker request issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=pmm-speaker-request) (optional)
* [ ] [Organic social promotion issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=social-gtm-organic) (optional)
* [ ] [Paid digital promotion issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=mktg-promotion-template) (optional)

## If partner is sharing leads:
- [ ] GitLab and partner to determine first touch and follow up
   - Best practice: joint lead follow up: Partner does 3 touches within 2 weeks following the event (a touch is a voicemail and email).  Qualified leads with immediate opportunity will be deal reg as they come in.  After 14 day follow up, full list with follow up notes will be provided to GitLab for list upload and further nurturing by GL
- [ ] [Program tracking issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-program-tracking)
- [ ] [List clean & upload request issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=event-clean-upload-list). (In lead upload, record the opt-in T&Cs used in the upload Issue and in the upload template, set Opt-in = True.
* [ ] [Add to nurture issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-add-nurture)

## If receiving recording and have distribution of webinar:
* [ ] [Pathfactory upload issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) (only open if we receive and can use recording, webinar DRI responsible for upload)
* [ ] [Pathfactory track issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) (only open if we receive and can use recording)

## Post event tasks:
- [ ] [Ordering a swag appreciation gift](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/community-appreciation/) for speakers
- [ ] [Request add to Resources](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition)

#### Opt in language applicable to all scenarios below and all geographies:
_By registering for this GitLab and [partner name] event, you agree that GitLab and [Partner name] may email you about their products, services and events. You may opt-out at any time by unsubscribing in future emails or visiting the relevant company's preference center._

In order to mark leads as Opt-in = TRUE, a record of the terms and conditions the leads agreed to upon having their data collected must be recorded. Check the terms of service wording has been recorded in the upload issue before opting in leads to receive marketing communications. No ToS, no Opt-in. Period. To find the appropriate language, refer to Marketing Rules and Consent Language
If there are any records who have opted out of contact for any reason, define that on the spreadsheet by selecting Opt-in = FALSE
Leave Opt-In empty if no other option is available

/label ~"mktg-status::wip" ~"Webinar - Channel Partner"  ~"Webinar"

```

# Sponsored Webinars
{: #sponsored-webinars .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
---

*A sponsored webcast is hosted on an external vendor platform (i.e. DevOps.com); this is a paid tactic. The vendor is responsible for driving registration, moderating and hosting the webinar on their platform, and delivering a lead list after the event. The goal of a sponsored webinar is net new leads - we do not promote to our existing database as it is a paid activity.*

### Process in GitLab to organize epic & issues
{: #sponsored-webinar-project-management}
<!-- DO NOT CHANGE THIS ANCHOR -->
The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (FMM) creates the main tactic issue
1. Project owner (FMC) creates the epic to house all related issues (code below)
1. Project owner (FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Epic code and issue templates
{: #sponsored-webinar-epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->

```
<!-- Name this epic: Sponsored Webcast - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Project Owner:** 
* **FMC/FMS:** 
* **Type:** Sponsored Webcast
* **Official Name:** 
* **Registration URL:** 
* **Persona (choose one):** `Practitioner, Manager, or Executive`
* **Use Case (choose primary):** `CI, VC&C, DevSecOps, Other` (FY21-22 focus on CI and VC&C)
* **Sales Segment (choose primary):** `Large, Mid-Market, or SMB`
* **Sales Region (choose one):** `AMER, EMEA, APAC`
* **Sales Territory (optional, if specific):** 
* **Goal:** `Please be specific on the metric this is meant to impact.`
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner/FM to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project Owner to provide a description of the user journey - from communication plan, to what the user experiences upon receipt, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project Owner/FMM add whatever additional notes are relevant here]

## Issue creation
If you are Field Marketer, see next section below for the issues you need to create. 

* [ ] Program Tracking
  - [If tactic owner is Campaigns Team](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-program-tracking)
* [ ] [Write copy issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=write-copy)
* [ ] [Follow up email issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-email-followup) 
* [ ] [List clean and upload issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list)
* [ ] [Add to nurture issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-add-nurture)
* [ ] [Pathfactory Upload issue created](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) (*optional*) 

## Issue Creation for Field Marketing 
Please delete the generic issue creation section above if you are a FMC creating this. 
* [ ] [Program Tracking](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=program-tracking) - FMC creates, assigns to FMC
* [ ] [Write copy issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=write-copy) - FMC creates, assigns to FMM
* [ ] [Follow Up Email issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_email_followup) - FMC creates, assigns to FMM (issue will be triaged)
* [ ] [Add to Nurture](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_add_nurture) - FMC creates, assigns to FMM (issue will be triaged)
* [ ] [List Clean and Upload](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMC creates, assigns to FMM and MOps
* [ ] [Optional: FM Pathfactory Asset Upload and Track Creation Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Pathfactory_Request_Template) - FMM creates, assigns to FMC

Add the team label to indicate the team running the event (Example: Field Marketing, Corporate Marketing)   

/label ~"mktg-status::wip" ~"Webcast - Sponsored" 
```

☝️ *Note on campaign utm format: we avoid using special characters due to issues in the past passing UTMs from Bizible to SFDC, the basis for attribution reporting.*

# Virtual Conferences
{: #virtual-conferences .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
---

*A virtual conference is hosted on an external vendor platform (i.e. Hopin); this is a paid tactic. GitLab pays a sponsorship fee to receive a virtual booth and often speaking session or panel presence. The goal of a sponsored virtual conference is net new leads - we do not promote to our existing database as it is a paid activity.*

**Presence of a virtual booth is a requirement for the virtual event to be considered a Virtual Conference.** [Link to Marketo program template that will be cloned.](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/ME7624A1)

### Process in GitLab to organize epic & issues
{: #virtual-conference-project-management}
<!-- DO NOT CHANGE THIS ANCHOR -->

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (Corp/FMM) creates the main tactic issue
1. Project owner (Corp/FMC) creates the epic to house all related issues (code below)
1. Project owner (Corp/FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (Corp/FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (Corp/FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Epic code and issue templates
{: #virtual-conference-epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->

```
<!-- Name this epic: Sponsored Virtual Conference - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Corp Events/Field Marketing Manager/Requester:** 
* **Field Marketing Coordinator:** 
* **Type:** Conference
* **Official Name:** 
* **Date(s):**  [YYYY-MM-DD] 
* **Registration URL:** 
* **Sales Segment (choose primary):** `Large, Mid-Market, or SMB`
* **Sales Region (choose one):** `AMER, EMEA, APAC`
* **Sales Territory (optional, if specific):** 
* **Goal:** `Please be specific on the KPI this is meant to impact.`
* **Budget:** 
* **Campaign Tag:**  
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project owner to provide a description of the user journey - from communication plan, to what the user experiences upon receipt, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project owner add whatever additional notes are relevant here]

## Issue creation
If you are a Field Marketer, see below for issue creation. 
* [ ] Program Tracking
  - [If tactic owner is Campaigns Team](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-program-tracking)
* [ ] [Write copy issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=write-copy)
* [ ] [Follow up email issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-email-followup) 
* [ ] [List clean and upload issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list)
* [ ] [Add to nurture issue](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-add-nurture)
* [ ] [Pathfactory Upload issue created](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) (*optional*)

## Issue Creation for Field Marketing 
Please delete the generic issue creation section above if you are a FMC creating this. 
* [ ] [Program Tracking](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=program-tracking) - FMC creates, assigns to FMC
* [ ] [Write copy issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=write-copy) - FMC creates, assigns to FMM
* [ ] [Follow Up Email issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_email_followup) - FMC creates, assigns to FMM (issue will be triaged)
* [ ] [Add to Nurture](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_add_nurture) - FMC creates, assigns to FMM (issue will be triaged)
* [ ] [List Clean and Upload](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMC creates, assigns to FMM and MOps
* [ ] [Optional: FM Pathfactory Asset Upload and Track Creation Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Pathfactory_Request_Template) - FMM creates, assigns to FMC


<details>
<summary>Corporate Marketing Activation: Expand below for quick links to issues to be created and linked to the epic.</summary>

* [ ] Activate*: [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Corp creates, assignment in issue
* [ ] Activate*: [Blog Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Corp creates, assignment in issue
* [ ] Activate*: [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - Corp creates, assignment in issue

</details>

**Everything with an * is optional: create the optional issues only if we plan to use those outbound activation channels*

Add the team label to indicate the team running the event (Example: Field Marketing, Corporate Marketing)    

/label ~"mktg-status::wip" ~"Virtual Conference" 
```

# Executive Roundtables
{: #executive-roundtables .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
---

*An executive roundtable is a gathering of high level CxO attendees run as an open discussion between the moderator/host, GitLab expert and delegates. There usually aren't any presentations, but instead a discussion where anyone can chime in to speak. The host would prepare questions to lead discussion topics and go around the room asking delegates questions to answer. The project owner (field marketing) is responsible for creating the epic and related issue creation, and keeping timelines and DRIs up-to-date. Mareting Ops is responsible for uploading the list to our database.* 

### Process in GitLab to organize epic & issues
{: #executive-roundtable-project-management}
<!-- DO NOT CHANGE THIS ANCHOR -->

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (FMM) creates the main tactic issue
1. Project owner (FMC) creates the epic to house all related issues (code below)
1. Project owner (FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Epic code and issue templates
{: #executive-roundtable-epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->

```
<!-- Name this epic: Executive Roundtable - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Project Owner/Field Marketing Manager:** 
* **FMC/FMS:** 
* **Type:** Executive Roundtable 
* **Official Name:** 
* **Date(s):** 
* **Registration URL:** 
* **Sales Segment (choose primary):** `Large, Mid-Market, or SMB`
* **Sales Region (choose one):** `AMER, EMEA, APAC`
* **Sales Territory (optional, if specific):** 
* **Goal:** `Please be specific on the metric this is meant to impact.`
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner/FM to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project Owner/FMM to provide a description of the user journey - from communication plan, to what the user experiences upon reciept, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project Owner/FMM add whatever additional notes are relevant here]

## Issue creation
* [ ] [Program Tracking - FM](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=program-tracking) - FMC creates, assigns to FMC
* [ ] [Write copy issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=write-copy) - FMC creates, assigns to FMM
* [ ] [Sales Nominated Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_sales_nominated) - FMC creates, assigns to the FMM (issue will be triaged)
* [ ] [Email Invitation Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_email_invite) - FMC creates one for each invitation requested, assigns to FMM (issue will be triaged)
* [ ] [Follow Up Email issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_email_followup) - FMC creates, assigns to FMM (issue will be triaged)
* [ ] [Add to Nurture](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_add_nurture) - FMC creates, assigns to FMM (issue will be triaged)
* [ ] [List Clean and Upload](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMC creates, assigns to FMM and MOps
* [ ] [Optional: FM Pathfactory Asset Upload and Track Creation Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Pathfactory_Request_Template) - FMM creates, assigns to FMC

/label ~"mktg-status::wip" ~"Field Marketing" ~"Executive Roundtable"
```

# Vendor Arranged Meetings
{: #vendor-arranged-meetings .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->
---

*A vendor arranged meeting is used for campaigns where a third party vendor is organizing one-to-one meetings with prospect or customer accounts, ideally connecting our team with target accounts that are challenging to set meetings with directly. This does not include meetings set internally by GitLab team members. An example would be a "speed dating" style meeting setup where a vendor organized meetings with prospects of interest to GitLab. The project owner (field marketing commonly) is responsible for creating the epic and related issue creation, and keeping timelines and DRIs up-to-date. Mareting Ops is responsible for uploading the list to our database.* 

### Process in GitLab to organize epic & issues
{: #vendor-arranged-meeting-project-management}
<!-- DO NOT CHANGE THIS ANCHOR -->

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (FMM) creates the main tactic issue
1. Project owner (FMC) creates the epic to house all related issues (code below)
1. Project owner (FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Epic code and issue templates
{: #vendor-arranged-meeting-epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->

```
<!-- Name this epic: Vendor Arranged Meeting - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Project Owner:** 
* **FMC/FMS:** 
* **Type:** Vendor Arranged Meetings 
* **Official Name:** 
* **Registration URL:** 
* **Sales Segment (choose primary):** `Large, Mid-Market, or SMB`
* **Sales Region (choose one):** `AMER, EMEA, APAC`
* **Sales Territory (optional, if specific):** 
* **Goal:** `Please be specific on the metric this is meant to impact.`
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner/FM to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project Owner/FMM to provide a description of the user journey - from communication plan, to what the user experiences upon reciept, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project Owner/FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Program Tracking - FM](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=program-tracking) - FMC creates, assigns to FMC
* [ ] [Write copy issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=write-copy) - FMC creates, assigns to FMM
* [ ] [Follow Up Email issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_email_followup) - FMC creates, assigns to FMM (issue will be triaged)
* [ ] [Add to Nurture](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_add_nurture) - FMC creates, assigns to FMM (issue will be triaged)
* [ ] [List Clean and Upload](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMC creates, assigns to FMM and MOps 
* [ ] [Optional: FM Pathfactory Asset Upload and Track Creation Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Pathfactory_Request_Template) - FMM creates, assigns to FMC

**Optional: create the optional issues only if we have rights to recording and content is worth gating*

/label ~"mktg-status::wip" ~"Field Marketing" ~"Vendor Arranged Meetings"
```

## Actions after the external virtual event
{: #post-external-virtual-event .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

###  Posting external virtual event recordings to youtube
{: #post-youtube}
<!-- DO NOT CHANGE THIS ANCHOR -->
Follow this handbook documentation on how to [upload external webcast recordings](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#uploading-conversations-to-youtube) to the [GitLab branded Youtube channel](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#channels).

**This process must be completed by the tactic owner.**

*Note: because these requests move through the Campaign Managers currently and their focus and priority is planning, implementing, and optimizing top-funnel campaign strategies, there is a [5 Business Day SLA](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#turnaround-time-and-slas).*

For immediate follow up emails, it is recommended to point directly to the GitLab Youtube link (uploaded by the tactic owner) to avoid delays in the send.

If an upload to Pathfactory (and addition to a track), is *required*, the tactiic owner must open a [Pathfactory Upload](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload) issue and **include the GitLab Youtube link**, as well as a [Pathfactory Track](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track) issue to have the asset added to a track.


### Gating external webcasts
{: #post-gating}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Posting external webcasts WITHOUT a tech/alliance partner
{: #post-gating-non-alliance}
<!-- DO NOT CHANGE THIS ANCHOR -->
The recording meets all of the following criterias:
1. Content solidifies GitLab use case or existing campaign messaging.
2. Future gated page has an omni-channel (min 2, 1 out of the 2 has to be paid) promotion plans. The issue for the  promotion plans has to be linked to the gating request.

#### Posting external webcasts WITH a tech/alliance partner
{: #post-gating-with-alliance}
<!-- DO NOT CHANGE THIS ANCHOR -->
The recording meets all of the following criterias:
1. Select or High priority partner: Listed as high priority on the [Alliances Technology Dashboard](https://docs.google.com/spreadsheets/d/1-EE7vChGkDeyJxoM-LjVmUdwYwboxBmq8_42hjHGw_w/edit#gid=0) or is a Select channel partner.
2. Content solidifies GitLab use case or existing campaign messaging.
3. Future gated page has an omni-channel (min 2, 1 out of the  2 has to be paid) promotion plans. The issue for the  promotion plans has to be linked to the gating request.

OR  

Ungated video garners 550 youtube views within the first 7 days of posting.

*Note: The 550 min threshold is based on the avg of top 10 videos on gitlab branded youtube channel between 8/11/20 - 8/18/20.*
