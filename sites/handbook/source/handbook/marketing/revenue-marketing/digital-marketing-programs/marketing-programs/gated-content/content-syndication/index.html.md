---
layout: markdown_page
title: "Content Syndication"
---

## Overview
Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#content-syndication](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#content-syndication)

### Process in GitLab to organize epic & issues
Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#content-syndication](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#content-syndication-process)

### Code for epic
Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#content-syndication](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#content-syndication-epic-code)
