---
layout: handbook-page-toc
title: "Account Based Strategy"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## [This page has moved](/handbook/marketing/account-based-marketing/)
Please reference the updated page here (and bookmark for future reference): [https://about.gitlab.com/handbook/marketing/account-based-marketing/](/handbook/marketing/account-based-marketing/)
