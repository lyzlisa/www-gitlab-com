const fs = require('fs');

// Take a number from the command line
const numberOfImagesToDelete = parseInt(process.argv[2]);

// If the number is not a number, or is less than 1, exit
if (isNaN(numberOfImagesToDelete) || numberOfImagesToDelete < 1) {
    console.log('Please enter a number greater than 0');
    process.exit(1);
}

// Check if ../tmp/unused-images.txt exists
if (!fs.existsSync('./tmp/unused-images.txt')) {
    console.log('No unused images found. Please run `yarn scan-unused-images` first.');
    process.exit();
}

// If ../tmp/unused-images.txt exists, read it
const unusedImages = fs.readFileSync('./tmp/unused-images.txt').toString().split('\n');

// For as many images as numerOfImagesToDelete, delete the image
for (let i = 0; i < numberOfImagesToDelete; i++) {
    const image = unusedImages[i];
    if (image) {
        console.log(`Deleting ${image}`);
        fs.unlinkSync(image);
    }
}

